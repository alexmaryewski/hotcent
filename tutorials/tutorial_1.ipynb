{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial #1\n",
    "\n",
    "This notebook illustrates how [Hotcent](https://gitlab.com/mvdb/hotcent)\n",
    "can be used to generate the electronic part of a DFTB parametrization\n",
    "for silicon (Si), i.e.\n",
    "\n",
    "- the eigenvalues and Hubbard values of the free atom\n",
    "- the necessary two-center Hamiltonian and overlap integrals\n",
    "\n",
    "The electronic parameters already allow to perform band structure\n",
    "calculations, as shown in Tutorial #2.\n",
    "\n",
    "Total energy calculations also require the repulsive energy.\n",
    "While Hotcent also allows to build the repulsive potential from the\n",
    "(confined) atomic densities, this would not work well in DFTB.\n",
    "To compensate for e.g. the few-center approximations in the DFTB Hamiltonian,\n",
    "it is needed to treat the repulsive potential in an empirical way,\n",
    "fitting it to reproduce experimental or theoretical data.\n",
    "Different tools are available for this purpose, one of which is the\n",
    "[Tango](https://gitlab.com/mvdb/tango) code.\n",
    "\n",
    "## Atomic eigenvalues\n",
    "\n",
    "The script below shows how to set up the (all-electron) atomic DFT calculator\n",
    "in Hotcent, which performs radial-symmetric DFT calculations on isolated atoms.\n",
    "The eigenvalues of the $3s$ and $3p$ valence states are printed out at the end."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from ase.units import Ha\n",
    "from hotcent.atomic_dft import AtomicDFT\n",
    "\n",
    "atom = AtomicDFT('Si',\n",
    "                 xc='LDA',\n",
    "                 configuration='[Ne] 3s2 3p2',  # the electron configuration we want to use\n",
    "                 valence=['3s', '3p'],  # these will be the valence states\n",
    "                 scalarrel=False,  # for Si we don't need (scalar) relativistic effects\n",
    "                 perturbative_confinement=False,\n",
    "                 txt='-',  # for printing to stdout\n",
    "                 )\n",
    "atom.run()\n",
    "atom.plot_Rnl('Si_Rnl_free.png')  # plot the radial parts of the valence orbitals\n",
    "atom.plot_rho('Si_rho_free.png')  # plot the valence orbital densities and total electron density\n",
    "\n",
    "print('=======================================')\n",
    "for nl in atom.valence:\n",
    "    e = atom.get_eigenvalue(nl)\n",
    "    print(nl, '[Ha]:', e, '[eV]:', e * Ha)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Atomic Hubbard values\n",
    "\n",
    "While the above eigenvalues reflect the first-order (linear) change of the\n",
    "isolated atom energy as a function of the orbital occupancy, the corresponding\n",
    "Hubbard values $U$ reflect the second-order (quadratic) response. By calculating\n",
    "$U$ as the difference of the electron affinity (EA) and ionization energy (IE),\n",
    "as done in the script below, we'll be calculating this second derivative for\n",
    "the highest occupied subshell ($3p$) using second-order central differences.\n",
    "\n",
    "Considering the LDA functional we are using, the calculated IE and EA compare\n",
    "well with the [NIST](\n",
    "https://webbook.nist.gov/cgi/cbook.cgi?ID=C7440213&Units=SI&Mask=20#Ion-Energetics)\n",
    "reference values (IE = 8.15 eV, EA = 1.39 eV)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from ase.units import Ha\n",
    "from hotcent.atomic_dft import AtomicDFT\n",
    "from hotcent.confinement import PowerConfinement\n",
    "\n",
    "energies = {}\n",
    "for occupation, kind in zip([2, 1, 3], ['neutral', 'cation', 'anion']):\n",
    "    atom = AtomicDFT('Si',\n",
    "                     xc='LDA',\n",
    "                     configuration='[Ne] 3s2 3p%d' % occupation,\n",
    "                     valence=['3s', '3p'],\n",
    "                     scalarrel=False,\n",
    "                     # Add a very weak confinement to aid anion convergence:\n",
    "                     confinement=PowerConfinement(r0=40., s=4),\n",
    "                     perturbative_confinement=False,\n",
    "                     txt=None,\n",
    "                     )\n",
    "    atom.run()\n",
    "    energies[kind] = atom.get_energy()\n",
    "\n",
    "EA = energies['neutral'] - energies['anion']  # the electron affinity\n",
    "IE = energies['cation'] - energies['neutral']  # the ionization energy\n",
    "U = IE - EA  # the (3p) Hubbard value\n",
    "\n",
    "for value, label in zip([EA, IE, U], ['EA', 'IE', 'U']):\n",
    "    print('%2s [Ha]: %.6f    [eV]: %.6f' % (label, value, value*Ha))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For convenience you can also use the ```get_hubbard_value``` function instead:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.units import Ha\n",
    "from hotcent.atomic_dft import AtomicDFT\n",
    "from hotcent.confinement import PowerConfinement\n",
    "\n",
    "atom = AtomicDFT('Si',\n",
    "                 xc='LDA',\n",
    "                 configuration='[Ne] 3s2 3p2',\n",
    "                 valence=['3s', '3p'],\n",
    "                 scalarrel=False,\n",
    "                 # Add a very weak confinement to aid anion convergence:\n",
    "                 confinement=PowerConfinement(r0=40., s=4),\n",
    "                 perturbative_confinement=False,\n",
    "                 txt=None,\n",
    "                 )\n",
    "\n",
    "# Like above, we use a central difference scheme\n",
    "# with changes in the occupation of +/- one electron\n",
    "U = atom.get_hubbard_value('3p', scheme='central', maxstep=1)\n",
    "print('%2s [Ha]: %.6f    [eV]: %.6f' % ('U', value, value*Ha))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Two-center integrals\n",
    "\n",
    "Now we'll generate the Slater-Koster tables and save them in [SKF format](\n",
    "http://www.dftb.org/fileadmin/DFTB/public/misc/slakoformat.pdf)\n",
    "together with the eigenvalues, Hubbard values and subshell occupancies\n",
    "in the free atom. The Slater-Koster tables are obtained by calculating\n",
    "the necessary Hamiltonian and overlap integrals (here: $ss\\sigma$, $sp\\sigma$,\n",
    "$pp\\sigma$ and $pp\\pi$) for a pair of Si atoms oriented along the $z$ axis.\n",
    "\n",
    "The required wave functions and atomic densities are, in turn, obtained\n",
    "by running atomic DFT calculations with added confinement.\n",
    "Harmonic confinement potentials $V_\\mathrm{conf} = (r/r_0)^2$ are arguably\n",
    "the simplest and also the most common choice. Typical (though not necessarily\n",
    "optimal) choices for the confinement radii are $r_0 = 2 R_{\\mathrm{cov}}$\n",
    "for the valence states and somewhat higher values for the confined density\n",
    "(here $3 R_{\\mathrm{cov}}$).\n",
    "\n",
    "The integrations are performed for a range of interatomic separations,\n",
    "where $0.4$ $a_0$ is a common choice for the minimal distance. The maximal\n",
    "distance (here $0.4 + 600 \\cdot 0.02 = 12.4$ $a_0$) should be large enough\n",
    "for the integrals to approach zero."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from ase.data import atomic_numbers, covalent_radii\n",
    "from ase.units import Bohr\n",
    "from hotcent.atomic_dft import AtomicDFT\n",
    "from hotcent.confinement import PowerConfinement\n",
    "from hotcent.offsite_twocenter import Offsite2cTable\n",
    "\n",
    "# Use standard, rule-of-thumb confinement potentials\n",
    "rcov = covalent_radii[atomic_numbers['Si']] / Bohr\n",
    "conf = PowerConfinement(r0=3 * rcov, s=2)\n",
    "wf_conf = {\n",
    "    '3s': PowerConfinement(r0=2 * rcov, s=2),\n",
    "    '3p': PowerConfinement(r0=2 * rcov, s=2),\n",
    "}\n",
    "\n",
    "atom = AtomicDFT('Si',\n",
    "                 xc='LDA',\n",
    "                 configuration='[Ne] 3s2 3p2',\n",
    "                 valence=['3s', '3p'],\n",
    "                 scalarrel=False,\n",
    "                 confinement=conf,\n",
    "                 wf_confinement=wf_conf,\n",
    "                 perturbative_confinement=False,\n",
    "                 txt=None,\n",
    "                 )\n",
    "atom.run()\n",
    "atom.plot_Rnl('Si_Rnl_conf.png')\n",
    "atom.plot_rho('Si_rho_conf.png')\n",
    "\n",
    "rmin, dr, N = 0.4, 0.02, 600\n",
    "off2c = Offsite2cTable(atom, atom)\n",
    "off2c.run(rmin, dr, N, superposition='density', xc='LDA')\n",
    "\n",
    "# Write the Slater-Koster tables to file (without two-body repulsion at this point).\n",
    "# This file also stores the eigenvalues, Hubbardvalues, occupations, as well as the\n",
    "# so-called spin-polarization error (the magnetization energy of the atom, which we\n",
    "# don't need to consider here).\n",
    "off2c.write(\n",
    "    eigenvalues={'3s': -0.397837, '3p': -0.153163},\n",
    "    hubbardvalues={'3s': 0.244242, '3p': 0.244242},\n",
    "    occupations={'3s': 2, '3p': 2},\n",
    "    spe=0.,\n",
    ")\n",
    "off2c.plot('Si-Si_offsite2c.png')\n",
    "print('Done!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare the radial parts of the orbitals and the orbital densities for the\n",
    "free and confined atoms and have a look at the plotted Slater-Koster tables.\n",
    "The resulting 'repulsionless' SKF file (```Si-Si_offsite2c.skf```) can be used\n",
    "for band structure calculations.\n",
    "\n",
    "<img src=\"./Si_rho_free.png\" style=\"width: 500px;\">\n",
    "\n",
    "<img src=\"./Si_rho_conf.png\" style=\"width: 500px;\">\n",
    "\n",
    "<img src=\"./Si-Si_offsite2c.png\" style=\"width: 500px;\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Attachments",
  "kernelspec": {
   "display_name": "venv_devel",
   "language": "python",
   "name": "venv_devel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
